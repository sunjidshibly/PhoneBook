-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2016 at 09:14 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phonebook_sun`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonebook`
--

CREATE TABLE `phonebook` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_deleted` bit(1) DEFAULT b'0',
  `coverpage` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phonebook`
--

INSERT INTO `phonebook` (`id`, `name`, `phoneNumber`, `address`, `email`, `is_deleted`, `coverpage`) VALUES
(1, 'Mr. B', '01677074196', '1400 E K', 'sm.sun3@ymail.com', b'0', '527C868C9284958A22F9E4D448BDDA12.JPG'),
(2, 'sun2', '0167895649', '47 ws', 'sm.sun3@ymail.com', b'0', 'images.jpg'),
(6, 'abcd', '01678956499', '1400 E K', '12345@gmail.com', b'0', 'images.jpg'),
(17, 'sun', '01677074196', 'Monipur, Dhaka-1216', 'sm.sun3@ymail.com', b'1', '527C868C9284958A22F9E4D448BDDA12.JPG'),
(18, 'sun2', '01677074196', 'Monipur, Dhaka-1216', 'sm.sun3@ymail.com', b'1', '160203182545-bitch-face-large-169.jpg'),
(19, 'Apon Rahman', '01677074196', '1400 E K', 'apondj@gmail.com', b'1', '160203182545-bitch-face-large-169.jpg'),
(20, 'Raju', '0167895649', 'Monipur, Dhaka-1216', 'sm.sun3@ymail.com', b'1', 'landscape-1439714614-celebrity-face-mashups-taylor-swift-emma-watson.jpg'),
(21, 'Raju', '01678956499', 'Monipur, Dhaka-1216', 'apondj@gmail.com', b'0', 'images.jpg'),
(22, 'Apon Rahman', '01677074196', '1400 E K', 'enamul.nantu@gmail.com', b'1', '4242435-face.jpg'),
(23, 'sun', '01678956499', 'Monipur, Dhaka-1216', 'sm.sun3@ymail.com', b'0', '160203182545-bitch-face-large-169.jpg'),
(24, 'Enamul', '01673314770', 'Monipur, Dhaka-1216', 'sun8496@gmail.com', b'0', '4242435-face.jpg'),
(25, 'Rfs', '01671331011', 'monipur', 'mubasshirhossain@gmail.com', b'0', 'landscape-1439714614-celebrity-face-mashups-taylor-swift-emma-watson.jpg'),
(26, 'Mr. P', '01672691472', 'Hindol, taltola', 'sssunmoon002@gmail.com', b'0', 'images.jpg'),
(27, 'sun', '01673314770', '131 East Kazipara', 'sun8496@gmail.com', b'0', '4242435-face.jpg'),
(28, 'Anna', '01678956499', '47 ws', 'apondj@gmail.com', b'0', '160203182545-bitch-face-large-169.jpg'),
(29, 'Obama', '01678956499', '47 ws', 'apondj@gmail.com', b'0', '527C868C9284958A22F9E4D448BDDA12.JPG'),
(30, 'bdxghfggvb', '01678956499', '47 ws', 'apondj@gmail.com', b'0', 'landscape-1439714614-celebrity-face-mashups-taylor-swift-emma-watson.jpg'),
(32, 'Dr caprio', '01678956499', '47 ws', 'apondj@gmail.com', b'0', '4242435-face.jpg'),
(33, 'Miss S', '01678956499', '47 ws', 'apondj@gmail.com', b'0', '160203182545-bitch-face-large-169.jpg'),
(34, 'Mr.D', '01678956499', '47 ws', 'apondj@gmail.com', b'0', '4242435-face.jpg'),
(35, 'president', '01678956499', '47 ws', 'apondj@gmail.com', b'0', '527C868C9284958A22F9E4D448BDDA12.JPG'),
(36, 'Shibly', '01678956499', '47 ws', 'apondj@gmail.com', b'1', 'IMG_sunjid.shibly.mohammad_1459889390.JPG'),
(37, 'Shibly', '01678956499', '47 ws', 'apondj@gmail.com', b'0', 'IMG_sunjid.shibly.mohammad_1459889390.JPG'),
(38, 'Shibly', '01678956499', '47 ws', 'apondj@gmail.com', b'0', 'IMG_sunjid.shibly.mohammad_1459889390.JPG'),
(39, 'Apon Rahman', '01670189321', 'Monipur, Dhaka-1216', 'apondj@gmail.com', b'0', '11709532_10207117120026759_6745373642607410456_n_1459890967.jpg'),
(40, 'Rfs', '01671331011', 'monipur, Dhaka', 'mubasshirhossain@gmail.com', b'0', '12241416_10206822096703705_6049888665358300823_n_1459891058.jpg'),
(41, 'Zakir Hasan', '01723936882', 'rupnagar r/a', 'zhs3015@gmail.com', b'0', 'ing_logo_1462332209.jpg'),
(42, 'Zakir Hasan sumon', '01611330114', 'mirpur', 'sun8496@gmail.com', b'0', '6_1462338708.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phonebook`
--
ALTER TABLE `phonebook`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phonebook`
--
ALTER TABLE `phonebook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
