<!DOCTYPE html>
<html>
<head>
      
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Add Telephone Directory</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>

    <title>Add Telephone Directory</title>

</head>
<body>
     <div class="container-fluid">
          <div class="container">
             <h2>Add Telephone Directory</h2>
          </div>
      </div>
    <div class="container">
    <div class="row">
        
        <center><h3><a href="index.php"> Home</a></h3></center>
    
        <form action="createfile.php" method="post" enctype="multipart/form-data">
            <div>
            <label for="name">Name</label>
            <input id="firstName" name="name" type="text" class="form-control" required="required" />
            </div>
            
            <div>
            <label for="phoneNumber">Phone Number</label>
            <input id="phoneNumber" name="phoneNumber" type="text" class="form-control" required="required" />
            </div>
            
            <div>
            <label for="address">Address</label>
            <input id="address" name="address" type="text" class="form-control"  />
            </div>
            
            <div>
            <label for="email">email</label>
            <input id="email" name="email" type="email" class="form-control"  />
            </div>
            
            <div>
             <label for="coverpage">Enter Cover Photo</label>
             <input id="coverpage" name="coverpage" type="file" class="form-control-file"  />
            </div>
            <br>
            <button class="btn btn-default" type="submit">Add</button>
            <button class="btn btn-default" type="reset">Reset</button>
           
  
        </form>
    </div>
</div>

 <div class="container-fluid">
         
          <div class="container">
              <h5>Copyright&copy2016</h5>
              <h5>Developed By- SunjidShibly</h5>
          </div>
        
      </div>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.9.0.min.js"></script>
    <script src="Scripts/knockout-3.1.0.js"></script>
    <script src="Scripts/index.js"></script>
</body>
</html>

