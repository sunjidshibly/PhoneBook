<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title>Trash List</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--my sytle link-->
    <link href="../assets/mobirise/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>

  </head>
    <body>
      <!--body field start-->
      <div class="container-fluid">
          <div class="container">
              <h2>Phone Directory: Trash List</h2>
          </div>
      </div>
      <div class="container">
        <div class="section">     
        
        <?php 
        
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."phonebook_sun".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
    
         
         use App\Phonebook\Phonebook;
        
         
         $obj6= new Phonebook();
         $trash_data=$obj6->trash_index();
        ?>
        
        
         
            <center><h3><a href="index.php">Go back to PhoneBook</h3></a>
            <table class="table table-striped">
            <thead>
                <tr id="firstrow">
                    <th>SL</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               
                    <?php foreach ($trash_data as $phonebook_trash): ?>
                     <tr>
                    <td><span><?php echo $phonebook_trash['id'];?></span></td>
                    <td><span><img src="Resources/img/<?php echo $phonebook_trash['coverpage'];?>" height="50" width="50"></span></td>
                    <td><span><?php echo $phonebook_trash['name'];?></span></td>
                    <td><span><?php echo $phonebook_trash['phoneNumber'];?></span></td>
                    <td>
                        <a href='undo_trash.php?id=<?php echo $phonebook_trash['id'];?>'>Restore</a>&nbsp; 
                        <a href='delete.php?id=<?php echo $phonebook_trash['id'];?>'>Delete</a>
                    </td>
                     </tr>
                     <?php endforeach; ?>
                
            </tbody>
            </table>
                            <br>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <a href="undo_trash_all.php"><button class="btn btn-default" type="button">Restore All</button></a>
                <a href="delete_all.php"><button class="btn btn-default" type="button">Delete All</button></a>
            </div>
       <div class="col-md-4"></div>
              
            </center>
          </div>
        </div>
       <br> 
       
      <div class="container-fluid">
          <div class="container">
         
              <h5>Copyright&copy2016</h5>
              <h5>Developed By- SunjidShibly</h5>
          </div>
          
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
    
</html>
