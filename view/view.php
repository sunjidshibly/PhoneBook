<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>View Contact</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>

  </head>
  <body>
      <!--body field start-->
      <div class="container-fluid">
          <div class="container">
              <h2>View Information</h2>
          </div>
      </div>
          <div class="container">
              <div class="section"> 
                  <div class="row">
                      <div class="col-md-4"></div>
                  <div class="col-md-4">
                 <?php 
        
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."phonebook_sun".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
    
         
         use App\Phonebook\Phonebook;

         $obj2= new Phonebook();
         $view_data=$obj2->view($_GET);

       
        ?>
                 
           
           <table class="table table-striped">
                <?php
         foreach($view_data as $view):
         ?>
            
  <tr>
      <th colspan="2"><span><center><img src="Resources/img/<?php echo $view['coverpage'];?>" height="250" width="250"></center></span></th>
  </tr>
   <tr>
    <th>ID</th>
    <th>&nbsp&nbsp<b><?Php echo $view['id'];?></b></th>
  </tr>
  <tr>
    <td>Name</td>
    <td >&nbsp&nbsp;<b><?Php echo $view['name'];?></b></td>
  </tr>
    <tr>
    <td>Cell No</td>
    <td>&nbsp&nbsp<b><?php     echo $view['phoneNumber'];              // echo implode(", ",unserialize($phno['phoneno']));?></b></td>
  </tr>
    <tr>
    <td>Address</td>
    <td>&nbsp&nbsp<b><?Php echo $view['address'];?></b></td>
  </tr>
    <tr>
    <td>Email</td>
    <td>&nbsp&nbsp<b><?Php echo $view['email'];?></b></td>
  </tr>

</table>
              
       
                   
                    <form action="mail.php" method="post">
                       <!--  <input type="hidden" name="id" value="<?php //echo $view['id']; ?>">
                        <input type="hidden" name="id" value="<?php //echo $view['name']; ?>">
                        <input type="hidden" name="id" value="<?php// echo $view['phoneNumber']; ?>">
                        <input type="hidden" name="id" value="<?php// echo $view['address']; ?>">
                        <input type="hidden" name="id" value="<?php// echo $view['email']; ?>">-->
                        <input type="email" name="to_email" class="form-control" value="<?Php echo $view['email'];?>" placeholder="Enter Email" required="required"/>
                       <input type="hidden" name="view_data" value="<?php echo base64_encode(serialize($view)); ?>"  />
                        
                         <button class="btn btn-default" type="submit">Email to Friend</button> 
                         <br><br><br> <a href="index.php"><button class="btn btn-danger" type="button">Back</button></a>
                    </form>
          <?php endforeach;?>
      </div>
     </div>
              </div> </div> 
      <div class="container-fluid">
          <div class="container">
         
              <h5>Copyright&copy2016</h5>
              <h5>Developed By- SunjidShibly</h5>
          </div>
          
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>