<?php
ini_set("display_errors","On");
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."phonebook_sun".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

    use App\Phonebook\Phonebook;
    use App\Message\Message;


$pdf = new Phonebook();
$pdfs = $pdf->index();
$trs = "";

?>


                
                <?php
                $slno =0;
                foreach($pdfs as $mpdf):
                    $slno++;
                    $trs .="<tr>";
                    $trs .="<td>".$slno."</td>";
                    $trs .="<td>".$mpdf['name']."</td>";
                    $trs .="<td>".$mpdf['phoneNumber']."</td>";
                    $trs .="<td>".$mpdf['address']."</td>";
                    $trs .="<td>".$mpdf['email']."</td>";
                    // $trs .="<td><img src=../Resources/img/".$mpdf['coverpage']." height='50' width='50' /></td>";
                    $trs .="</tr>";
                 endforeach;   
                ?>


<?php

$html = <<<BITM
<!DOCTYPE html>
<html>
    <head>
        <title>Phonebook List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        </style>
    
    </head>
    
    <body>
        <h1>Phonebook List</h1>
     
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    
                    <th>Name &dArr;</th>
                    <th>Phone Number &dArr;</th>
                    <th>Address &dArr;</th>  
                    <th>Email &dArr;</th>
                   
                     
                
                </tr>
            </thead>
            <tbody>
        
              echo $trs;
        
        
        
        </tbody>
        </table>
       
        
            

    </body>
</html>
BITM;
?>
<?php

require_once $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."phonebook_sun".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php';

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

