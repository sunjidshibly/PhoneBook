<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title>Phone Directory</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--my sytle link-->
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>

  </head>
    <body>
      <!--body field start-->
      <div class="container-fluid">
          <div class="container">
              <h2>PhoneBook</h2>
          </div>
      </div>
      <div class="container">
        <div class="section">     
        
        <?php 
        
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."phonebook_sun".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
    
         
         use App\Phonebook\Phonebook;
        
         
         $obj= new Phonebook();
         $data=$obj->index();
        ?>
        
        
         
            <center><h3><a href="create.php">Add New </a>| Download as <a href="">PDF |</a><a href=""> XL</a></h3></center>
            <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               
                    <?php foreach ($data as $phonebook): ?>
                     <tr>
                    <td><span><?php echo $phonebook['id'];?></span></td>
                    <td><span><img src="Resources/img/<?php echo $phonebook['coverpage'];?>" height="50" width="50"></span></td>
                    <td><span><?php echo $phonebook['name'];?></span></td>
                    <td><span><?php echo $phonebook['phoneNumber'];?></span></td>
                    <td>
                        <a href='update.php?id=<?php echo $phonebook['id'];?>'>Edit</a>&nbsp;
                        <a href='view.php?id=<?php echo $phonebook['id'];?>'>View</a>&nbsp;
                        <a href='trashfile.php?id=<?php echo $phonebook['id'];?>'>Trash</a>&nbsp;
                         
                    </td>
                     </tr>
                     <?php endforeach; ?>
                
            </tbody>
            </table>
            <br>
            <div class="col-md-5"></div>
            <div class="col-md-2">
            <a href="trash_index.php"><button class="btn btn-default" type="button">Trash List</button></a> 
            </div>
       <div class="col-md-5"></div>
          </div>
          
        </div>
        <br>
       
      <div class="container-fluid">
          <div class="container">
         
              <h5>Copyright&copy2016</h5>
              <h5>Developed By- SunjidShibly</h5>
          </div>
          
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
    
</html>
