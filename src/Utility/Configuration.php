<?php

namespace App\Utility;

class Configuration {
    
    const HOSTNAME      = 'localhost';
    const DBNAME        = 'phonebook_sun';
    const DBUSER        = 'root';
    const DBPASSWORD    = '';
    
    const UPLOAD_DIR    = DIRECTORY_SEPARATOR."phonebook_sun".DIRECTORY_SEPARATOR.'View'.DIRECTORY_SEPARATOR.'Resources'.DIRECTORY_SEPARATOR;
    const WEBROOT       = 'http://localhost/phonebook_sun/';
    const IS_SECURE     = FALSE;
    
    const DEBUG_MODE    = TRUE;
}
